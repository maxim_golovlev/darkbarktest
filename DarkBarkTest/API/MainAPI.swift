import Foundation
import Alamofire

protocol MainAPI {
    
    func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, completion: ServerResult?)
}

extension MainAPI {
    
    func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, completion: ServerResult?) {
        
        Alamofire.request(url, method: type, parameters: parameters, headers: nil).responseJSON { (response) in
            
            guard let response = response.result.value as? [String: AnyObject] else {
                completion?( .error (message: "Something wrong. Please try again later."))
                return
            }
            
            completion?( .success (response: response))
        }
    }
}
