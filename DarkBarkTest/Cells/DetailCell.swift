//
//  DetailCell.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class DetailCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var label: UILabel!
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    func configure(with title: String?) {
        label.text = title
    }
}
