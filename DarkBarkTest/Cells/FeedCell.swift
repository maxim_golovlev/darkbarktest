//
//  FeedCell.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import AlamofireImage

class FeedCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    func configure(currency: CurrencyItem) {
        
        let urlString = APIKeys.flagImage(code: currency.charCode)
        if let url = URL.init(string: urlString) {
            imageView.af_setImage(withURL: url)
        }
        titleLabel.text = currency.charCode
        subtitleLabel.text = "\(currency.value) (₽, RUB)"
    }
    
    override func prepareForReuse() {
        imageView.image = #imageLiteral(resourceName: "placeholder")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
