//
//  CurrencyManager.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import PromiseKit

struct CurrencyManager: MainAPI {
    
    private init() {}
    
    static let shared = CurrencyManager()
    
    func fetchCurrencyList() -> Promise<[CurrencyItem]> {
        
        let urlString = APIKeys.dailyCurrencyList
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            sendRequest(type: .get, url: urlString, parameters: nil, completion: { (response) in
                
                switch response {
                    case let .success(response: json):
                        guard let currencyDict = json["Valute"] as? [String : [String: AnyObject]] else { reject(ResponseError.withMessage("Valute is not valid key for currency")); return }
                        
                        let currency = currencyDict.values.flatMap{ CurrencyItem.init(dict: $0) }
                        fullfill(currency)
                    case let .error(message: msg):
                        reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
}
