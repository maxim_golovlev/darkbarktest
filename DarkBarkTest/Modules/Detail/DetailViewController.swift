//
//  DetailViewController.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

protocol DetailViewProtocol: BaseView {
  func didLoadCurrency(currency: CurrencyItem)
}

class DetailViewController: UIViewController {
  
  // MARK: - Public properties
  
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.allowsSelection = false
            tableDirector = TableDirector.init(tableView: tableView)
        }
    }
    lazy var presenter:DetailPresenterProtocol = DetailPresenter(view: self)
  
  // MARK: - Private properties
  
    private var tableDirector: TableDirector!
    
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    presenter.fetchCurrency()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Display logic
  
  // MARK: - Actions
  
  // MARK: - Overrides
    
  // MARK: - Private functions
    
    func string(value: Any) -> String {

        if let string = value as? String {
            return string
        } else if let number = value as? Int {
            return "\(number)"
        } else if let number = value as? Double {
            return "\(number) (₽, RUB)"
        } else {
            return ""
        }
    }
}

extension DetailViewController:  DetailViewProtocol {
    func didLoadCurrency(currency: CurrencyItem) {
        
        tableDirector.clear()
        
        let mirror = Mirror(reflecting: currency)
        let section = TableSection()
        let rows = mirror.children.flatMap{ TableRow<DetailCell>.init(item: "\(CurrencyItem.discription(name: $0.label)) - \(string(value: $0.value))" )  }
        
        section.append(rows: rows)
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
}
