//
//  DetailPresenter.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit

protocol DetailPresenterProtocol: class {
  weak var view:DetailViewProtocol? { get set }
    func fetchCurrency()
    func setCurrency(currency: CurrencyItem?)
}

class DetailPresenter {
  
  // MARK: - Public variables
  weak var view:DetailViewProtocol?
  
  // MARK: - Private variables
    
    private var currency: CurrencyItem?
  
  // MARK: - Initialization
  init(view:DetailViewProtocol) {
    self.view = view
  }
}

extension DetailPresenter: DetailPresenterProtocol {
    func fetchCurrency() {
        guard let currency = currency else { return }
        view?.didLoadCurrency(currency: currency)
    }
    
    func setCurrency(currency: CurrencyItem?) {
        self.currency = currency
    }
}
