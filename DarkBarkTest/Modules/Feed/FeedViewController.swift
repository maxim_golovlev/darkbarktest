//
//  FeedViewController.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit

protocol FeedViewProtocol: BaseView {
    func currencyDidUpload(currency: [CurrencyItem])
}

class FeedViewController: UIViewController {
  
  // MARK: - Public properties
  
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var presenter:FeedPresenterProtocol = FeedPresenter(view: self)
  
  // MARK: - Private properties
    
    private var currency = [CurrencyItem]()
    private let cellId = "cellId"
    private let cellSpacing: CGFloat = 5
    private var upcomingSize: CGSize?
    private lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        return rc
    }()
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureCollectionView()
    presenter.fetchDailyCurrency()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Display logic
    
    private func configureCollectionView() {
        collectionView.addSubview(refreshControl)
        collectionView.register(UINib.init(nibName: "FeedCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        collectionView.addGestureRecognizer(longPressGesture)
    }
  
  // MARK: - Actions
    
    @objc func pullToRefresh() {
        presenter.fetchDailyCurrency()
    }
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else {
                break
            }
            collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            collectionView.endInteractiveMovement()
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
  
  // MARK: - Overrides
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        upcomingSize = size
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
  // MARK: - Private functions
    
    private func sizeForCell() -> CGSize {
        let orientation = UIDevice.current.orientation
        let scrWidth = upcomingSize != nil ? upcomingSize!.width : AppScreen.width
        let portrWidth = scrWidth/2 - cellSpacing/2
        let otherWidth = scrWidth/3 - cellSpacing
        
        switch orientation {
        case .portrait:
            return CGSize(width: portrWidth, height: portrWidth)
        default:
            return CGSize(width: otherWidth, height: otherWidth)
        }
    }
}

extension FeedViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return sizeForCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
}

extension FeedViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currency.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        if let cell = cell as? FeedCell {
            cell.configure(currency: currency[indexPath.item])
        }
        return cell
    }
}

extension FeedViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            
            vc.presenter.setCurrency(currency: currency[indexPath.item])
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        currency.swapAt(sourceIndexPath.item, destinationIndexPath.item)
    }
}

extension FeedViewController:  FeedViewProtocol {
    func currencyDidUpload(currency: [CurrencyItem]) {
        self.currency = currency
        collectionView.reloadData()
        refreshControl.endRefreshing()
    }
}
