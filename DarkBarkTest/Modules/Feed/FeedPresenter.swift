//
//  FeedPresenter.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit

protocol FeedPresenterProtocol: class {
    weak var view:FeedViewProtocol? { get set }
    func fetchDailyCurrency()
}

class FeedPresenter {
  
  // MARK: - Public variables
  weak var view:FeedViewProtocol?
  
  // MARK: - Private variables
  
  // MARK: - Initialization
  init(view:FeedViewProtocol) {
    self.view = view
  }
}

extension FeedPresenter: FeedPresenterProtocol {
    func fetchDailyCurrency() {
        view?.startLoading()
        
        CurrencyManager.shared.fetchCurrencyList()
            .then { (currency) -> Void in
                self.view?.currencyDidUpload(currency: currency)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
}
