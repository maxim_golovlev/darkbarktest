//
//  CurrencyItem.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct CurrencyItem {
    
    let id: String
    let numCode: String
    let charCode: String
    let nominal: Int
    let name: String
    let value: Double
    let prevValue: Double
    
    init?(dict: [String: AnyObject]) {
        
        guard let id = dict["ID"] as? String else { return nil }
        self.id = id
        numCode = dict["NumCode"] as? String ?? ""
        charCode = dict["CharCode"] as? String ?? ""
        nominal = dict["Nominal"] as? Int ?? 0
        name = dict["Name"] as? String ?? ""
        value = dict["Value"] as? Double ?? 0
        prevValue = dict["Previous"] as? Double ?? 0
    }
    
    static func discription(name: String?) -> String {
        
        guard let name = name else {return ""}
        
        switch name {
            case "id": return "ID"
            case "numCode": return "Number Code"
            case "charCode": return "Char Code"
            case "nominal": return "Nominal"
            case "name": return "Name"
            case "value": return "Current price"
            case "prevValue": return "Previous price"
            default: return ""
        }
    }
}
