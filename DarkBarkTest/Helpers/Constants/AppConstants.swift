//
//  AppConstants.swift
//  DarkBarkTest
//
//  Created by Admin on 03.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

enum APIKeys {
    static let dailyCurrencyList = "https://www.cbr-xml-daily.ru/daily_json.js"
    static func flagImage(code: String) -> String {
        return "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/\(code.lowercased()).png"
        
    }
}

struct AppScreen {
    static let scale = UIScreen.main.scale
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
    static let pixel = 1 / UIScreen.main.scale
    static let navbarHeight = 64
    static let tabbarHeight = 49
}
